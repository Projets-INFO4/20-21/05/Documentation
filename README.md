# Documentation

## Aborted repositories
The repositories for Minnow Server and Mongoose Server are aborted. 
They contain WIP importations and implementations of their libraries 
on FreeRTOS.

The Rasbery repository contains continuous WIP branch of the client side.
It allowed us to test out the client side during the implentations of the 
STM32 servers
## Final repository
The final repository containing all the fonctional source code is in STM32F7_CycloneServer.
Client side is implemented in Javascript and can be found in 
/CycloneServer/ressources/www 
with the corresponding ressources (html and images).

## Server Side (STM32F7 in C)

You can find [Full STM32 Documentation](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/20-21/05/STM32F7_CycloneServer/-/blob/master/DOCUMENTATION.md "Documentation") in the STM32 repository

## Client Side (Javascript,html)

The Client Side works similarly as an Single Page Application.
#### socket.js
socket.js contains the entry point of the client side. It creates the websocket 
and call the function to add the tables (Full and short).
#### sketch.js
sketch.js contains all the function for the sketch animations and create the 
sketch view of the state of the elevator.
          checkAnimationCabin() is the entry point for the animation of the 
          cabin and is called in the interpret.js functions
#### interpret.js
interpret.js contains all the function to interpret the datas for the sketch 
animation. It calls the different sketch.js function depending on 
the currentState and the lastState of the elevator.
#### fullTable.js
fullTable.js contains all the function to add datas to the first table 
(containing and displaying all the datas received).
#### shortTable.js
shortTable.js contains all the function to display the latest data received 
in the second table.
#### modal.js
modal.js contains the function to display the help information.
#### main.js
main.js contains the function called by the socket.js entry point to send 
datas to the different structure of the other .js files



